import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import requests # Getting Webpage content
from bs4 import BeautifulSoup as bs # Scraping webpages
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
# Visualization
# import matplotlib.style as style # For styling plots #
# from matplotlib import pyplot as mp # For Saving plots as images #

import json
import csv


def tags_and_numbers_summary(link ='https://stackoverflow.com/tags'):
    """Retrieves statistics from url for most popular tags"""
    response = requests.get(link)
    soup = bs(response.content, 'html.parser')
    body = soup.find('body')
    topic_tags = body.find_all('a', class_='post-tag')
    topics = [i.text for i in topic_tags]

    for node in soup.select("#tags-browser>div>:last-child"):
        questions = node.select_one(":first-child").text
        info = node.select_one(":last-child").text

    tag_numbers = body.find_all(class_='mt-auto grid jc-space-between fs-caption fc-black-300')
    questions_numbers = [x.text for x in tag_numbers] #l2
    total_number_questions = []
    today_questions = []
    week_questions = []
    for string_with_stat in questions_numbers:
        tmp = string_with_stat.split()
        total_number_questions.append(tmp[0])
        today_questions.append(tmp[2])
        week_questions.append(tmp[5])
    return topics, total_number_questions, today_questions, week_questions


def write_statisctics_to_csv(names, total, today, week, file_name):
    """Writes data to csv"""
    with open(file_name, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(names)
        writer.writerow(total)
        writer.writerow(today)
        writer.writerow(week)
    data_sof = pd.read_csv(file_name)
    return data_sof


def draw_some_plots(current_table, x):
    """Draws nice graphs with statistics, saves them to files"""
    # plt.subplot(311)
    fig = plt.figure(figsize=(10, 7))
    plt.title('Total number of questions per 36 most popular tags on StackOverflow')
    y0 = current_table.iloc[0]
    plt.ylabel('total number of questions')
    plt.xticks(range(len(x)), x, rotation=80)
    plt.plot(x, y0)
    plt.savefig('first_plot.png')


    # plt.subplot(312)
    fig = plt.figure(figsize=(10, 7))
    y1 = current_table.iloc[1]
    plt.title('Number of qustions today')
    plt.ylabel('today\'s number of questions')
    plt.xticks(range(len(x)), x, rotation=80)
    plt.plot(x, y1)
    plt.savefig('second_plot.png')


    # plt.subplot(313)
    fig = plt.figure(figsize=(10, 7))
    y2 = current_table.iloc[2]
    plt.title('Questions\' number for the week')
    plt.ylabel('weekly number of questions')
    plt.xticks(range(len(x)), x, rotation=80)
    plt.plot(x, y2)
    plt.savefig('third_plot.png')


def main():
    """Main function, that does almost everything. Scrapes pages and creates files with questions, views, votes, etc"""
    response = requests.get('https://stackoverflow.com/tags')
    # Parsing html data using BeautifulSoup
    soup = bs(response.content, 'html.parser')
    body = soup.find('body')
    lang_tags = body.find_all('a', class_='post-tag')  # retrieving language tags ,like c, python, etc
    languages = [i.text for i in lang_tags]
    names = tags_and_numbers_summary()[0]
    total = tags_and_numbers_summary()[1]
    today = tags_and_numbers_summary()[2]
    week = tags_and_numbers_summary()[3]
    file_name = 'summary_tags_stats.csv'
    write_statisctics_to_csv(names, total, today, week, file_name)
    current_table = write_statisctics_to_csv(names, total, today, week, file_name)
    draw_some_plots(current_table, names)

    # Let's generate links to questions
    # change some names in order to fit url
    names[3] = 'c%23'  # was c#
    names[8] = 'c%2b%2b'  # was c++

    # generate links to questions corresponding to tag name
    questions_tagged = ['https://stackoverflow.com/questions/tagged/' + x for x in names]

    # MAIN PART
    #Now we are going to page with questions for each tag
    for link in questions_tagged:
        nameoftag = names[questions_tagged.index(link)] #will be reflected in filenames
        if nameoftag == 'c%23': # change these names, so that filenames would be c#_questions.csv,not c%23_questions.csv
            nameoftag = 'c#'
        if nameoftag == 'c%2b%2b':
            nameoftag = 'c++'
        res = requests.get(link)
        soup = bs(res.text, "html.parser")
        questions_data = {
            "questions": []
        }
        questions = soup.select(".question-summary")
        for que in questions:
            q = que.select_one('.question-hyperlink').getText()
            vote_count = que.select_one('.vote-count-post').getText()
            views = que.select_one('.views').attrs['title']
            additional_tags = que.select_one('.tags').getText()
            questions_data['questions'].append({
                "question": q,
                "views": views,
                "vote_count": vote_count,
                "additional_tags": additional_tags
            })
        json_data = json.dumps(questions_data)
        list_of_rows = []
        question = ['question']
        views = ['views']
        vote_count = ['vote_count']
        additional_tags = ['additional_tags']
        our_string = json_data[16:]
        for row in our_string.split('\"question\"')[1:]:
            q = row.split('\"views\"')[0]
            question.append(q.split(':')[1].split('\"')[1])
            v = row.split('\"views\"')[1].split('\"vote_count\"')[0]
            views.append(int(v.split('views')[0].strip(':')[2]))
            vo = int(row.split('\"vote_count\"')[1].split()[1].split('\"')[1])
            vote_count.append(vo)
            adt = row.split('\"additional_tags\": \"\\n')[1].split('\\n')[0]
            additional_tags.append(adt)
        good_rows = [ [question[i],views[i], vote_count[i], additional_tags[i]] for i in range(len(views))]

        #OPTION 1: create separate files with last questions for each tag
        csv_filename_for_stats = nameoftag + '_qustions.csv'
        with open(csv_filename_for_stats, 'w', newline='') as myfile:
            wr = csv.writer(myfile)
            for grow in good_rows:
                wr.writerow(grow)

        #OPTION 2: create 1 file for all data
        csv_filename_for_stats = 'all_qustions_summary.csv'
        with open(csv_filename_for_stats, 'a+', newline='') as myfile:
            wr = csv.writer(myfile)
            for grow in good_rows:
                wr.writerow(grow)

        #Finally, let's see the content of the last file
    all_questions_pd = pd.read_csv('all_qustions_summary.csv')
    print(all_questions_pd.head())


if __name__ == '__main__':
    main()