import argparse


def print_sq(args):
    n = args.left
    for i in range(n):
        print(str(i) + ' ' + str(i ** 2))


def main():
    parser = argparse.ArgumentParser(description='Mess')
    n1 = parser.add_argument(type=int, dest='left',
                             help='Number 1')
    parser.add_argument('--left', type=int, default=n1,
                        help='Number 1')
    args = parser.parse_args()
    print_sq(args)


if __name__ == "__main__":
    main()